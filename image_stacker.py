import sys
import click
import pathlib

from multiprocessing import Pool
from tqdm import tqdm

import cv2 as cv
import numpy as np


################################################################################
def read_img_file_paths(input_dir_path):
    return [
        str(file)
        for file in pathlib.Path(input_dir_path).iterdir()
        if file.suffix.lower() in (".jpg", ".jpeg", ".png", ".bmp")]

################################################################################
def read_and_preprocess_img(img_file_path):
        img = cv.imread(img_file_path, cv.IMREAD_COLOR)
        return img.astype(np.float32) / 255.0

################################################################################
class ImageStacker:
    def __init__(
        self, warp_mode=cv.MOTION_HOMOGRAPHY, max_iters_num=2000,
        corr_coef_thresh=1e-5, gauss_filter_size=3):
        self.warp_mode = warp_mode
        self.stopping_criteria = (
            cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, max_iters_num,
            corr_coef_thresh)
        self.gauss_filter_size = gauss_filter_size
    
    def stack_imgs(self, img_file_paths, use_multiprocessing=True):
        if len(img_file_paths) < 2:
            raise ValueError(
                f"expected at least 2 images, got {len(img_file_paths)}")
        
        processes = None if use_multiprocessing else 1
        
        ref_img = read_and_preprocess_img(img_file_paths[0])
        ref_img_gray = cv.cvtColor(ref_img, cv.COLOR_BGR2GRAY)
        stacked_img = ref_img
        
        n_imgs = len(img_file_paths) - 1
        with Pool(processes=processes) as pool, tqdm(total=n_imgs) as pbar:
            try:
                pbar.set_description("stacking images")

                imgs_stream = map(
                    read_and_preprocess_img, img_file_paths[1:])
                job_results = (
                    pool.apply_async(
                        ImageStacker.warp_img_aligned,
                        (self, ref_img_gray, img))
                    for img in imgs_stream)
                
                for job_result in job_results:
                    warped_img = job_result.get()
                    stacked_img += warped_img
                    pbar.update()
                
                stacked_img /= len(img_file_paths)
                stacked_img = (stacked_img * 255.0).astype(np.uint8)
                
                return stacked_img
            except KeyboardInterrupt as _:
                return None
    
    def warp_img_aligned(self, ref_img_gray, img):
        warp_matrix_rows_num = 3
        if self.warp_mode != cv.MOTION_HOMOGRAPHY:
            warp_matrix_rows_num = 2
        warp_matrix = np.eye(warp_matrix_rows_num, 3, dtype=np.float32)
        
        img_gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        _, warp_matrix = cv.findTransformECC(
            img_gray, ref_img_gray, warp_matrix, self.warp_mode,
            self.stopping_criteria, None, self.gauss_filter_size)
        
        height, width, _ = img.shape
        img_warped = cv.warpPerspective(img, warp_matrix, (width, height))
        
        return img_warped


################################################################################
@click.command()
@click.argument(
    "input_dir_path", type=click.Path(exists=True))
@click.argument("output_file_path")
@click.option("-s", "--show", is_flag=True, help="Show the output image.")
def main(input_dir_path, output_file_path, show):
    """
    This application is licensed under the MIT license.
    
    Copyright (c) 2021 Milan Ondrasovic

    Runs image stacking algortihm on a given set of input images to enhance the
    quality of detail and suppress the effect of noise.
    """
    img_file_paths = read_img_file_paths(input_dir_path)
    img_stacker = ImageStacker()
    stacked_img = img_stacker.stack_imgs(img_file_paths)
    cv.imwrite(output_file_path, stacked_img)

    if show:
        cv.imshow("Preview: stacked image", stacked_img)
        cv.waitKey(0)
        cv.destroyAllWindows()
    
    return 0


################################################################################
if __name__ == '__main__':
    sys.exit(main())
